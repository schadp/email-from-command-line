
#!/bin/bash 

#starts by clearing the screen

clear

#makes the prompts a function
function email {
	#prompt for mutiple inputs
        read -p "Account Label: " account_label
        read -p "Email Address: " email_address
        read -sp "Email Password: " email_password

}

#makes the EOF and chmod into a function
function EOF {
	#HEREDOC writes .msmtprc in home directory
        # Make the two documents using EOF
        cat <<EOF > ~/.msmtprc
        #Gmail account
        defaults
        logfile ~/msmtp.log

        account $account_label
        auth on
        host smtp.gmail.com
        from $email_address
        auth on
        tls on
        tls_trust_file /usr/share/ca-certificates/mozilla/Equifax_Secure_CA.crt
        user $email_address
        password $email_password
        port 587

        account default : $account_label
EOF

	#make readable and writeable only to owner
        chmod 600 ~/.msmtprc

	#HEREDOC for ~/mailrc in users home directory (hidden)
        cat <<EOF > ~/.mailrc
        set sendmail="/usr/bin/msmtp"
        set message-sendmail-extra-arguments="-a gmail"
EOF

}

#uses an if then statment to determin how to run it if it needs to be install or config

#installation for sudo user only; only needs to be run once on a machine
#run as [emailscript2.sh install]
if [ $1 == "install" ]
then
        #check for sudo
            if [ $EUID -ne 0 ]; then
            echo ""
            echo "-- $0 must be run with sudo."
            echo ""
            exit 1
            fi
	
	#use of the first function
	email
	
	#update and install
        apt-get update
        apt-get install heirloom-mailx msmtp-mta -y
	
	#use of the second function
	EOF

#run with config at the end if user is not root or sudo
#run as [emailscript2.sh config]
else [ $1 == "config" ]

email
EOF

fi